Source: apollo
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 12~),
               default-jdk,
               javahelper,
               gradle-debian-helper
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/med-team/apollo
Vcs-Git: https://salsa.debian.org/med-team/apollo.git
Homepage: http://genomearchitect.org/

Package: apollo
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: genome annotation viewer and editor
 Apollo is a genome annotation viewer and editor. It was developed as a
 collaboration between the Berkeley Drosophila Genome Project (part of
 the FlyBase consortium) and The Sanger Institute in Cambridge, UK.
 Apollo allows researchers to explore genomic annotations at many levels
 of detail, and to perform expert annotation curation, all in a graphical
 environment. It was used by the FlyBase biologists to construct the
 Release 3 annotations on the finished Drosophila melanogaster genome,
 and is also a primary vehicle for sharing these annotations with the
 community. The Generic Model Organism Database (GMOD) project, which
 aims to provide a complete ready-to-use toolkit for analyzing whole
 genomes, has adopted Apollo as its annotation workbench.
